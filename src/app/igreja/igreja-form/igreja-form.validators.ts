import { FormControl } from '@angular/forms';

export class IgrejaValidators {

  static email (control: FormControl){

    let EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return EMAIL_REGEXP.test(control.value) ? null : {
      validateEmail: {
        valid: false
      }
    };
  }

  static cnpj (control: FormControl){

    let CNPJ_REGEXP = /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/;

    return CNPJ_REGEXP.test(control.value) ? null : {
      validarCnpj: {
        valid: false
      }
    };
  }


}
