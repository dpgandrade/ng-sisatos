import { Component, OnInit, OnDestroy }       from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute }             from '@angular/router';

import { Subscription, Observable } from 'rxjs/Rx';

import {Message} from 'primeng/primeng';

import { IgrejaService }          from '../igreja.service';
import { Igreja }                 from '../igreja';
import { IgrejaValidators }       from './igreja-form.validators';
//import { ComponentCanDeactivate } from '../pessoa.guard';

@Component({
  selector: 'app-igreja-form',
  templateUrl: './igreja-form.template.html',
  styleUrls: ['./igreja-form.template.css']
})
export class IgrejaFormComponent implements OnInit, OnDestroy {

  form: FormGroup;

  private igrejaIndex: number;
  private isNew: boolean = true;
  private igreja: Igreja;
  private subscription: Subscription;

  public title: string;
  public loading: boolean;
  public btnValue: string;

  msgs:    			     Message[] = [];
	alerts:						 Message[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private igrejaService: IgrejaService) { }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        this.loading = true;
        if (params.hasOwnProperty('id')) {
          this.title = 'Editar igreja';
          this.isNew = false;
          this.igrejaIndex = +params['id'];
          this.igrejaService.get(this.igrejaIndex).
               subscribe(response =>  {
                 this.setIgreja(response);
                 this.btnValue = 'save';
                 this.loading = false;
               })
        } else {
          this.title = 'Nova igreja';
          this.isNew = true;
          this.igreja = new Igreja();
          this.btnValue = 'add';
          this.loading = false;
        }
      }
    );
    this.initForm();
    console.log('sopa');
  }

  private setIgreja(igreja: Igreja){
       this.igreja = igreja;
  }

  private initForm() {
    this.form = this.formBuilder.group({
      status: ['A',  Validators.required],
      codigo: '',
      nome: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      cnpj: '', //['', IgrejaValidators.cnpj],
      telefone: '',
      email: '', //['',  IgrejaValidators.email],
      website:'',
      dtFundacao: ''
    });
  }

  onCancel() {
    this.navigateBack();
  }

  private navigateBack() {
    this.router.navigate(['/igreja']);
  }

  onSave() {
		this.loading = true;
    let result, acao, dtFundacao;
    const igrejaValue = this.form.value;

    dtFundacao = igrejaValue['dtFundacao'].match(/(\d+)/g)
    igrejaValue['dtFundacao']  = new Date(dtFundacao[0],dtFundacao[1]-1,dtFundacao[2]);

    if (this.isNew){
      acao   = ' inserida ';
      igrejaValue['status']  = 'A'; // status A no cadastro;
      igrejaValue['tipo']    = 'F';
    } else {
      acao   = ' editada ';
      igrejaValue['id']      = this.igrejaIndex;
    }

    result = this.igrejaService.save(igrejaValue);

    result.subscribe(data => {
      this.loading = false;
      this.showAlert({
        severity:'success',
        summary:'Igreja '+ acao +' com sucesso',
        detail:''
      });
    },
    err => {
      alert("Um erro ocorreu");
      console.log(err);
    });
  }

  inativar(igreja: Igreja){
    this.msgs.push({
      severity:'warn',
      summary:igreja.nome+ ' será inativada ',
      detail:'Dependendo dos vínculos existentes, está ação não será possível'
    });
  }

  showAlert(msgs: any) {
    this.alerts.push(msgs);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.form.dirty) {
      return confirm('Deseja mesma sair dessa página ?');
    }
    return true;
  }
}
