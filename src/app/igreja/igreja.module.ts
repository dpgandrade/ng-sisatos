import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }  from '@angular/router';
import { HttpModule }  from '@angular/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MessagesModule, GrowlModule} from 'primeng/primeng';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { InputTextModule } from 'primeng/primeng';
import { PasswordModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { DataTableModule, DropdownModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { MegaMenuModule} from 'primeng/primeng';
import { ChartModule } from 'primeng/primeng';

import {  IgrejaComponent }          from './igreja.component';
import {  IgrejaDashBoardComponent } from './igreja-dashboard/igreja-dashboard.component';
import {  IgrejaFormComponent }      from './igreja-form/igreja-form.component';
import {  igrejasRouting }           from './igreja.routing';
import {  IgrejaService }            from './igreja.service';

@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule,
      HttpModule ,

      MessagesModule,
      GrowlModule,
      ConfirmDialogModule,
      DataTableModule,
      DropdownModule,
      ChartModule,

      igrejasRouting
    ],
    declarations: [
      IgrejaComponent,
      IgrejaDashBoardComponent,
      IgrejaFormComponent
    ],
    providers: [
       IgrejaService, ConfirmationService
    ]
})
export class IgrejaModule {}
