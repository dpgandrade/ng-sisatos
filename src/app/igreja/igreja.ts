//import { Endereco } from '../endereco/endereco';

export class Igreja{
  id:          number;
  status:      string;
  tipo:        string;
  codigo:	     string;
  nome:	       string;
  cnpj:        string;
  telefone:	   string;
  email:       string;
  website:     string;
  imagem:      string;
  dtFundacao:  Date;
  obs:         string;

  endereco:    any;
}
