import { Routes, RouterModule } from '@angular/router';

import { IgrejaComponent }          from './igreja.component';
import { IgrejaDashBoardComponent } from './igreja-dashboard/igreja-dashboard.component';
import { IgrejaFormComponent }      from './igreja-form/igreja-form.component';
//import {  PessoaGuard }              from './pessoa.guard';

const IGREJAS_ROUTES: Routes = [
  { path: '',
    component: IgrejaComponent,
    children: [
        { path: '',    component: IgrejaDashBoardComponent },
        { path: 'add', component: IgrejaFormComponent },
        { path: ':id', component: IgrejaFormComponent}
  ]}
];

/* Por ser <const>, o nome do router tem inicial minuscula */
export const igrejasRouting = RouterModule.forChild(IGREJAS_ROUTES);
