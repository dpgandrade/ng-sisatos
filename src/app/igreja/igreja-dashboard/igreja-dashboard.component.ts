import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { AuthHttp } 			   from 'angular2-jwt';

import { Message } 						 from 'primeng/primeng';
import { ConfirmationService } from 'primeng/primeng';

import { Igreja }        from '../igreja';
import { IgrejaService } from '../igreja.service';

@Component({
	selector: 'app-igreja-dashboard',
	templateUrl: './igreja-dashboard.template.html',
  styleUrls: [ './igreja-dashboard.template.css']
})
export class IgrejaDashBoardComponent  implements OnInit{
  login:    any    = '';
	bg_color: string = '';
	igrejas:  Igreja[];

	totalIgrejas:  number;
	tituloGrafico: string;

	igrejaSelecionada: Igreja;
  msgs:    			     Message[] = [];
	alerts:						 Message[] = [];
  data: 						 any;
	loading:           boolean   = false;

	constructor(private igrejaService:  		 IgrejaService,
							private confirmationService: ConfirmationService,
		    			private router:	   		 			 Router){

				/*
				pessoaService.getPessoasGenero()
											.subscribe(response => {
												this.loading = true;
												this.grafPessoaGenero(response);
												this.setGeneros();
												this.loading = false;
											});
											*/
	}

	getIgrejas(): void {
		this.loading = true;
		this.igrejaService
		    .getIgrejas()
		    .then(result => {
					  this.igrejas      = result.igrejas as Igreja[];
					  this.totalIgrejas = result.total;
					  this.loading      = false;
				});
	}

	deleteConfirm(igreja: Igreja): void {
		this.confirmationService.confirm({
					message: 'Confirma  exclusão da igreja ' +igreja.nome+ '?',
					accept: () => {
							this.delete(igreja);
					}
			});
	}

	delete(igreja: Igreja): void{
		this.loading = true;
		this.igrejaService
		.delete(igreja.id)
		.then(() => {
		  this.igrejas = this.igrejas.filter(h => h !== igreja);
		  if (this.igrejaSelecionada === igreja) {
				this.igrejaSelecionada = null;
			}
			this.loading = false;
			this.showAlert({severity:'success',
										summary:igreja.nome+' foi excluída com sucesso',
									  detail:''});
		});

	}
	/*
	setGeneros(){
		this.generos.push({label:'Todos', value:null});
		this.generos.push({label:'Masculino', value:'M'});
		this.generos.push({label:'Feminino', value:'F'});
	}
*/
	/* construindo grafico */
	/*
	grafPessoaGenero(dados: any){
		 this.tituloGrafico = 'Pessoa por gênero';

		 let labels = [];
		 let data   = [];

		 for(let dado of dados){
			 labels.push(dado.genero);
			 data.push(dado.total);
		 }
		 this.data = {
					 labels: labels,
					 datasets: [
							 {
									 data: data,
									 backgroundColor: [
											 "#FF6384",
											 "#36A2EB"
									 ],
									 hoverBackgroundColor: [
											 "#FF6384",
											 "#36A2EB"
									 ]
							 }]
					 };
	}
	*/

	showAlert(msg: any) {
        this.alerts = [];
        this.alerts.push(msg);
  }

	ngOnInit(): void {
	  this.getIgrejas();
		this.login = JSON.parse(localStorage.getItem('user'));
		this.bg_color = this.login.nivel.color;
	}

}
