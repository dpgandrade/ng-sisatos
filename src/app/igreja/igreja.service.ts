import { Injectable, EventEmitter }    from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { contentHeaders } from '../common/headers';
import { enviroment }     from '../common/enviroment';

import { Igreja } from './igreja';

@Injectable()
export class IgrejaService {
  private igreja: Igreja;
  private url:    string = enviroment.API+'igrejas';

  constructor(private http: Http) { }

    getIgrejas() {
      return this.http.get(this.url, {headers: contentHeaders})
                 .toPromise()
                 .then(response => {
                   return {'igrejas':response.json().igrejas.data as Igreja[],
                           'total':response.json().total}
                 })
                 .catch(this.handleError);
    }

    get(id): Observable <Igreja>{
       return this.http.get(this.url+'/'+id, {headers: contentHeaders})
                 .map(response => response.json().data as Igreja)
                 .catch(this.handleError);
    }

    save(igreja: Igreja){
      let response: Observable<any>;
      if(igreja.id){
          response = this.http.put(this.url+'/'+igreja.id,
                                JSON.stringify(igreja),
                                {headers: contentHeaders})
                              .map(res => res.json().data)
                              .catch(this.handleError);
      }else{
        console.log(igreja);
        response = this.http.post(this.url,
                              JSON.stringify(igreja),
                              {headers: contentHeaders})
                            .map(res => res.json().data)
                            .catch(this.handleError);
      }
     return response;
   }

   delete(id: number): Promise<void> {
     return this.http.delete(this.url+'/delete/'+id, {headers: contentHeaders})
       .toPromise()
       .then(() => null)
       .catch(this.handleError);
   }

   /* Relatorios e Graficos */

   getIgrejasArea(): Observable <any>{
     return this.http.get(this.url+ '/getIgrejaByArea', {headers: contentHeaders})
               .map(response => response.json() )
               .catch(this.handleError);
   }

  serializer(objects: any){
    let data = [];
    let cont: number = 0;

    for (let obj of objects) {
       data.push(obj.data);
       cont++;
    }
    return { data: data, total: cont };
  }


  private handleError(error: any): Promise<any> {
    console.log('Ocorreu um erro'+error);
    return Promise.reject(error.message || error);
  }
}
