import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { AuthHttp } 			   from 'angular2-jwt';

import { Igreja }        from './igreja';
import { IgrejaService } from './igreja.service';

@Component({
	selector: 'app-igreja',
	template: `<router-outlet></router-outlet>`
})
export class IgrejaComponent  implements OnInit{

  constructor(){}

	ngOnInit(){

	}

}
