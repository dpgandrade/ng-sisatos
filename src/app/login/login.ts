export interface Login {
   user_id: number;
   pessoa_id: number;
   nivel_id: number;
   token: string;
}
