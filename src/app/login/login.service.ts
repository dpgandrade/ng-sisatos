﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { contentHeaders } from '../common/headers';
import { enviroment }     from '../common/enviroment';

@Injectable()
export class LoginService {
    public token: string;
    private user: any;
    private url = enviroment.API;

    constructor(private http: Http) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(login: string, senha: string) :Observable <any>{
	    return this.http.post(this.url + 'authenticate', JSON.stringify({login: login, password: senha}), {headers:contentHeaders})
            .map((response: Response) => {
                // reposta jwt token
                let token = response.json() && response.json().token;
                let user  = response.json().user;
                if (token) {

                    this.user = {'id': user.data.id,
                                 'login': user.data.login,
                                 'pessoa_id': user.data.pessoa.data.id,
                                 'nivel_id':  user.data.nivel.data.id,
                                 'pessoa': user.data.pessoa.data,
                                 'nivel' : user.data.nivel.data
                               };
                    console.log(this.user);

                    localStorage.setItem('token',token);
                    localStorage.setItem('user',  JSON.stringify(this.user));
                    return response;
                }
            })
            .catch((response: Response) =>{
                    return Observable.throw(response.json());
            });

    }

    getModulosByLogin() :Observable <any>{
      return this.http.get(this.url + 'nivel/getNivelByLogin', {headers:contentHeaders})
                      .map((response: Response) => {
                         let r     = this.serializer(response.json().nivel.data.modulos);
                         return { modulos: r.data, total: r.total };
                      });
    }

    serializer(objects: any){
      let data = [];
      let cont: number = 0;

      for (let obj of objects) {
         data.push(obj.data);
         cont++;
      }
      return { data: data, total: cont };
    }


    logout(): void {
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}
