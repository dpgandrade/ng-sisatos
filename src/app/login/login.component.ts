﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './login.service';

@Component({
    selector:    'app-login',
    templateUrl: './login.template.html',
    styleUrls:  ['./login.template.css'],
    providers:  [LoginService]
})

export class LoginComponent implements OnInit {
    loading: boolean = false;
    erro:    boolean = false;
    erroMsg: string  = '';

    constructor(
        private router: Router,
        private loginService: LoginService) {
    }

    ngOnInit() {
      this.loginService.logout();
    }

    login(event, lgn, senha) {
       event.preventDefault();
       this.loading = true;
       if(this.camposPreenchidos(lgn, senha)){
         this.loginService.login(lgn,senha)
              .subscribe(result => {
                 console.log(result);
                  if (result.status === 200) {
                    window.location.href = '/home';
                  }

              }, (err) =>{
                  this.erro = true;
                  this.erroMsg = err.error;
                  this.loading = false;
              });
       }
    }

    camposPreenchidos(lgn, senha){
      if(lgn != '' && senha != ''){
        return true;
      }  else {
        this.loading = false;
        this.erro    = true;
        this.erroMsg = 'Preencha os campos Login e Senha';
      }
    }
}
