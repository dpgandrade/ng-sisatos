export class Endereco {
  id:          number;
  cep:         string;
  logradouro:  string;
  numero:	     string;
  bairro:	     string;
  cidade:      string;
  uf:	         string;
  complemento: string;
  lat:         string;
  long:        string;

}
