import { Injectable, EventEmitter }    from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { contentHeaders } from '../common/headers';
import { enviroment }     from '../common/enviroment';

import { Pessoa } from './pessoa';

@Injectable()
export class PessoaService {
  private pessoa: Pessoa;

  private url: string = enviroment.API+'pessoas';
  pessoasModificadas = new EventEmitter<Observable <Pessoa[]>>();

  constructor(private http: Http) { }

    getPessoas() {
      return this.http.get(this.url, {headers: contentHeaders})
                 .toPromise()
                 .then(response => {
                   return {'pessoas':response.json().pessoas.data as Pessoa[],
                           'total':response.json().total}
                 })
                 .catch(this.handleError);
    }

    get(id): Observable <Pessoa>{
       return this.http.get(this.url+'/'+id, {headers: contentHeaders})
                 .map(response => response.json().data as Pessoa)
                 .catch(this.handleError);
    }

    save(pessoa: Pessoa){
      let response: Observable<any>;  
      if(pessoa.id){
          response = this.http.put(this.url+'/'+pessoa.id,
                                JSON.stringify(pessoa),
                                {headers: contentHeaders})
                              .map(res => res.json().data)
                              .catch(this.handleError);
      }else{
        response = this.http.post(this.url,
                              JSON.stringify(pessoa),
                              {headers: contentHeaders})
                            .map(res => res.json().data)
                            .catch(this.handleError);
      }
     return response;
   }

   delete(id: number): Promise<void> {
     return this.http.delete(this.url+'/delete/'+id, {headers: contentHeaders})
       .toPromise()
       .then(() => null)
       .catch(this.handleError);
   }

   private getUrl(id){
     return `${this.url}/${id}`;
   }

   /* Relatorios e Graficos */

   getPessoasGenero(): Observable <any>{
     return this.http.get(this.url+ '/getPessoasByGenero', {headers: contentHeaders})
               .map(response => response.json() )
               .catch(this.handleError);
   }

   /*
    getPessoas(): Promise<any> {
      return this.http.get(this.url, {headers: contentHeaders})
                 .toPromise()
                 .then(response => {
                  let data = this.serializer(response.json());
                  console.log(data);
                 })
                 .catch(this.handleError);
    }
    */


  getPessoa(id: number): Promise<Pessoa> {
    const url = `${this.url}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as Pessoa)
      .catch(this.handleError);

  }

  create(name: string): Promise<Pessoa> {
    return this.http
      .post(this.url, JSON.stringify({name: name}), {headers: contentHeaders})
      .toPromise()
      .then(res => res.json().data)
      .catch(this.handleError);
  }

  serializer(objects: any){
    let data = [];
    let cont: number = 0;

    for (let obj of objects) {
       data.push(obj.data);
       cont++;
    }
    return { data: data, total: cont };
  }


  private handleError(error: any): Promise<any> {
    console.log('Ocorreu um erro'+error);
    return Promise.reject(error.message || error);
  }
}
