//import { Endereco } from '../endereco/endereco';

export class Pessoa {
  id:                 number;
  nome:               string;
  email:              string;
  sexo:	              string;
  status:	            string;
  dtNascimento:       Date;
  imagem:	            string;

  obs:               string;
  codAux:            number;

  endereco: any;

}
