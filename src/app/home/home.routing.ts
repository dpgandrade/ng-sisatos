import { Routes, RouterModule } from '@angular/router';

import {  HomeComponent }          from './home.component';

const HOME_ROUTES: Routes = [
  { path: '', component: HomeComponent}
];

/* Por ser <const>, o nome do router tem inicial minuscula */
export const homeRouting = RouterModule.forChild(HOME_ROUTES);
