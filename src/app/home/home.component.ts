import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

@Component({
	selector: 'app-home',
	templateUrl: './home.template.html'
})
export class HomeComponent  implements OnInit{
  login: any;

  constructor(){

  }

	ngOnInit(){
	  this.login = JSON.parse(localStorage.getItem('user'));
	}

}
