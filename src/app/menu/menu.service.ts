import { Injectable, EventEmitter }    from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { contentHeaders } from '../common/headers';
import { enviroment }     from '../common/enviroment';

import { Menu } from './menu';

@Injectable()
export class MenuService {
  private menu: Menu;

  private url: string = enviroment.API+'menus/';

  constructor(private http: Http) {}

  getByLogin(): Observable <any>{
     let moduloAtual = JSON.parse(localStorage.getItem('moduloAtual'));
     return this.http.get(this.url+'getByLogin/'+moduloAtual.id, {headers: contentHeaders})
               .map(response => response.json().data)
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log('Ocorreu um erro'+error);
    return Promise.reject(error.message || error);
  }



}
