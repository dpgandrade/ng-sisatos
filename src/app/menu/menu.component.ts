import{ Component, EventEmitter } from '@angular/core';

import { MaterializeDirective, MaterializeAction } from 'angular2-materialize';

import{ AuthGuard }    from '../common/auth.guard';
import{ PessoaService} from '../pessoa/pessoa.service';
import{ Pessoa }       from '../pessoa/pessoa';
import{ LoginService}  from '../login/login.service';
import{ MenuService}   from './menu.service';

declare var jQuery:any;

@Component({
    selector:    'app-menu',
    templateUrl: './menu.template.html',
    styleUrls:  ['./menu.template.css']
})

export class MenuComponent {
  /* variavel que informa ao template se o modulo já foi selecionado pelo usuario */
  public moduloCarregado: boolean = false;
  /* variavel que contem o modulo atual que está no localStorage*/
  public moduloAtual: any = { id: 0 };
  /* variavel que informa ao template se o Menu já foi carregado */
  public showMenu: boolean = false;
 /* variavel com os menus do usuario */
  public menus = false;
  /* variavel com os modulos do nivel do usuario */
  public modulos = false;
  /* Variavel com a cor predominante do nivel. Está no banco de dados*/
  public bg_color: any;
  /* Variavel com informações do usuarios logado*/
  public login: any;

  constructor(private authGuard: AuthGuard,
              private pessoaService: PessoaService,
              private loginService: LoginService,
              private menuService: MenuService){
     this.login = authGuard.autenticado;
  }

  ngOnInit() {
    /* Testando se o usuario está autenticado */
    if(this.isAuth()){
      /* Variavel local login populada com dados do processo do login */
      let login = JSON.parse(localStorage.getItem('user'));
      /* Populando informações da pessoa logada */
      this.setPessoa(login.pessoa);
      /* Exibindo o Menu */
      this.showMenu = true;
      /* Setando a cor de fundo do menu, conforme Nivel do usuario*/
      this.bg_color = login.nivel.color;
      /* Buscando apenas os módulos referente ao nivel do login do usuario */
      this.loginService.getModulosByLogin()
                       .subscribe(response => {
                         /* Criando o objeto com todos os modulos encontrados */
                         this.modulos = response.modulos;
                         /* Caso o usuário só possua acesso a 1 modulo, ele já o carrega */
                         if(response.total == 1) this.loadModulo(response.modulos[0]);
                         /* Caso o Modulo Atual já tenha sido carregado */
                         if(localStorage.getItem('moduloAtual')) this.loadModulo(JSON.parse(localStorage.getItem('moduloAtual')));
                       })
    }
  }

  isAuth(){
    return this.authGuard.canActivate();
  }

  setPessoa(pessoa: Pessoa){
    this.login = pessoa;
  }

  /* Carrega/Muda o modulo do sistema */
  loadModulo(modulo): void {
    localStorage.setItem('moduloAtual', JSON.stringify(modulo));
    this.moduloAtual = modulo;
    this.loadMenuModulo();
  }
  /* Carrega/Muda os menus do usuário logado */
  loadMenuModulo(): void {
    let modulo = localStorage.getItem('moduloAtual');
    this.menuService.getByLogin()
                    .subscribe(response => {
                      this.menus           = response;
                      this.moduloCarregado = true;
                    });
  }

  logout(): void {
    return this.authGuard.logout();
  }
}
