import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { AuthGuard }      from './common/auth.guard';

const appRoutes: Routes = [
	{path: 'igreja',
	 loadChildren: 'app/igreja/igreja.module#IgrejaModule',
	 canActivate: [AuthGuard]},

	{path: 'pessoa',
	 loadChildren: 'app/pessoa/pessoa.module#PessoaModule',
	 canActivate: [AuthGuard]},

	 {path: 'home',
 	 loadChildren: 'app/home/home.module#HomeModule',
 	 canActivate: [AuthGuard]},


	{path: 'login',  component: LoginComponent},
	{path: 'logout', component: LoginComponent},

  {path: '', 			 redirectTo: 'home', pathMatch: 'full'},
	{path: '**',  	 redirectTo: 'home', pathMatch: 'full'}
];


export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
