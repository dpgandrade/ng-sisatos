import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public bg_color;

  ngOnInit(){
    if(localStorage.getItem('user')){
      let login     = JSON.parse(localStorage.getItem('user'));
      this.bg_color = login.nivel.color;
    }else{
      this.bg_color = '#2AA8F6';
    }
  }
}
